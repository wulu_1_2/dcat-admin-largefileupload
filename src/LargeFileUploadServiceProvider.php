<?php

namespace Hkw\LargeFileUpload;

use Dcat\Admin\Extend\ServiceProvider;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Hkw\LargeFileUpload\Form\LargeFile;

class LargeFileUploadServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

	public function init()
	{
		parent::init();
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'hkw-form-largefile');
        Admin::booting(function () {
            Form::extend('largefile', LargeFile::class);
        });

	}
}
