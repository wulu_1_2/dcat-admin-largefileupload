# Dcat Admin 大文件云直传Form扩展

### 功能说明
1. 大文件分片直传
2. 集成了七牛云、阿里云OSS
3. 预览目前只支持img,video,其他类型文件一律只显示上传文件名

####前置包
1. "dcat/laravel-admin": "~2.0",
2. "zgldh/qiniu-laravel-storage": "0.10.4"（使用七牛云需要）
3. "bmslaravel/aliyun-sts": "^1.1" （使用阿里云oss需要）

#### 七牛云：在filesystem.php 的 disks下增加下面代码，当默认文件驱动为qiniu时自动开启前端直传和大文件分片上传

```php
'qiniu' => [
    'driver'                 => 'qiniu',
    'domains'                => [
        'default' => env('QINIU_DOMAIN_DEFAULT'), //你的七牛域名
        'https'   => env('QINIU_DOMAIN_HTTPS'),         //你的HTTPS域名
        'custom'  => env('QINIU_DOMAIN_HTTPS'),                //Useless 没啥用，请直接使用上面的 default 项
    ],
    'access_key'             => env('QINIU_ACCESS_KEY'),  //AccessKey
    'secret_key'             => env('QINIU_SECRET_KEY'),  //SecretKey
    'bucket'                 => env('QINIU_BUCKET'),  //Bucket名字
    'notify_url'             => '',  //持久化处理回调地址
    'access'                 => 'public',  //空间访问控制 public 或 private
    'hotlink_prevention_key' => '', // CDN 时间戳防盗链的 key。 设置为 null 则不启用本功能。
    'region'                 => env('QINIU_REGION', 'z1')
],
```
####  阿里云oss： 安装bmslaravel/aliyun-sts扩展后发布配置文件sts.php，完善配置文件（里面的配置项需要进阿里云配置RAM授权策略，具体参考阿里云OSS直传手册），当默认文件驱动为oss时自动开启前端直传和大文件分片上传

### 使用方法

```php
$form->largefile('videos');
$form->largefile('videos')->limit(1); //限制数量，默认为0（不限制），多个会以英文逗号隔开提交
$form->largefile('videos')->accept('video/*,image/*');//类型限制
```



