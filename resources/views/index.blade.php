<div class="{{$viewClass['form-group']}}">

    <label class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}" id="{{$name}}_file_container">

        @include('admin::form.error')
        <div class="flex-wrap mt-1 file_list" limit_num="{{$limit}}">
            <div class="hkw_file_container">
                <div class="add_btn"></div>
            </div>
        </div>
        <input type="file" class="file_input" accept="{{$accept}}" style="display: none"/>
        <input type="hidden" name="{{$name}}" value="{{ old($column, $value) }}" {!! $attributes !!} />
        <progress class="file_progress" value="0" max="100" style="width: 100%;display: none"> 0% </progress>
        @include('admin::form.help-block')

    </div>
</div>

<!-- script标签加上 "init" 属性后会自动使用 Dcat.init() 方法动态监听元素生成 -->
<script init="{!! $selector !!}">
    var dir = "{{config('admin.upload.directory.file')}}";
    var img_domain;
    var uploaderClient = {
        upload(file){
        },
        next(percent){

        },
        success(url){

        },
        error(err){

        }
    };
    //七牛云
    @if(config('filesystems.default') == 'qiniu')
        img_domain = "{{config('filesystems.disks.qiniu.domains.default')}}";
        uploaderClient.upload = function(file){
            let qiniuClient = qiniu.upload(file, null, "{{$uptoken}}", {}, {
                region: {{$config['region']}},
                upprotocol: "{{request()->isSecure()?'https':'http'}}",
                chunkSize: 1
            });
            qiniuClient.subscribe({
                next: (res) => {
                    this.next(Math.floor(res.total.percent));
                },
                error: (res) => {
                    this.error(res);
                },
                complete: (res) => {
                    this.success(res.data.id);
                }
            });
        };

    @endif
    //阿里云oss
    @if(config('filesystems.default') == 'oss')
        img_domain = "{{config('sts.bucket_name')}}"+'.'+"{{config('sts.endpoint')}}";
        uploaderClient.upload = function(file){
            let ossClient = new OSS({
                region: "{{$region}}",
                // 从STS服务获取的临时访问密钥（AccessKey ID和AccessKey Secret）。
                accessKeyId: "{{$access_key_id}}",
                accessKeySecret: "{{$access_key_secret}}",
                // 从STS服务获取的安全令牌（SecurityToken）。
                stsToken: "{{$security_token}}",
                // 填写Bucket名称，例如examplebucket。
                bucket: "{{config('sts.bucket_name')}}",
            });
            let filename = getTimeId(8, 12);
            let fileext = file.name.substring(file.name.lastIndexOf('.') + 1);
            ossClient.multipartUpload(`${dir}/${filename}.${fileext}`, file, {
                progress: (p, cpt, res) => {
                    this.next(Math.floor(p * 100));
                },
                parallel: 3,
                partSize: 1024 * 1024,
            }).then(res => {
                this.success(res.name);
            }).catch(err => {
                this.error(err);
            });
        };

    @endif
    hkwLargeFileInit("{{$name}}_file_container",id,uploaderClient,img_domain);

</script>
